//
//  VideoPreviewView.swift
//  NBUFParser
//
//  Created by Alexander Kozlov on 17.04.2021.
//

import AVFoundation
import UIKit

class VideoPreviewView: UIView {
    public override func didMoveToSuperview() {
        super.didMoveToSuperview()
        videoPreviewLayer.videoGravity = .resizeAspectFill
    }
    
    public override class var layerClass: AnyClass {
        return AVCaptureVideoPreviewLayer.self
    }
    
    public var videoPreviewLayer: AVCaptureVideoPreviewLayer {
        return layer as! AVCaptureVideoPreviewLayer
    }
}
