//
//  ParserBase.swift
//  NBUFParser
//
//  Created by Alexander Kozlov on 10.04.2021.
//

import Foundation

struct Parser<T> {
    let run: (inout String) -> T?
}

func always<T>(_ t: T) -> Parser<T> {
    Parser<T> { _ in
        t
    }
}

extension Parser {
    static func never<T>() -> Parser<T> {
       Parser<T> { _ in
           nil
       }
    }
    
    func map<B>(_ f: @escaping (T)-> B) -> Parser<B> {
        Parser<B> { str in
            self.run(&str).map(f)
        }
    }
    
    func flatMap<B>(_ f: @escaping (T) -> Parser<B>) -> Parser<B> {
        Parser<B> { str in
            let original = str
            let matchA = self.run(&str)
            let parserB = matchA.map(f)
            guard let matchB = parserB?.run(&str) else {
                str = original
                return nil
            }
            return matchB
        }
    }
}

func zip<T, B>(_ t: Parser<T>, _ b: Parser<B>) -> Parser<(T, B)> {
    Parser<(T, B)> { str in
        let original = str
        guard let matchA = t.run(&str) else { return nil }
        guard let matchB = b.run(&str) else {
            str = original
            return nil
        }
        return (matchA, matchB)
    }
}

func oneOf<A>(_ p1: Parser<A>, _ p2: Parser<A>) -> Parser<A> {
    Parser<A> { str -> A? in
        if let match = p1.run(&str) {
            return match
        }
        if let match = p2.run(&str) {
            return match
        }
        return nil
    }
}
