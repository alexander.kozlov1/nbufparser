//
//  NbuQrParser.swift
//  NBUFParser
//
//  Created by Alexander Kozlov on 10.04.2021.
//

import Foundation

enum NbuCurrency: String, RawRepresentable {
    static let codeLength: Int = 3
    case uah = "UAH"
}

struct NbuAmount {
    let currency: NbuCurrency
    let amount: NSDecimalNumber
    
    var debugDescription: String {
        "\(currency.rawValue)\(amount)"
    }
}

struct NbuQrCode {
    let version: Int
    let coding: Int
    let function: String
    let fullName: String
    let account: String
    let amount: NbuAmount?
    let code: String
    let paymentPurpose: String
    
    var debugDescription: String {
        let amountDescription = amount == nil ? "nil" : amount!.debugDescription
        return """
        version = \(version)
        coding = \(coding)
        function = \(function)
        fullName = \(fullName)
        account = \(account)
        amount = \(amountDescription)
        code = \(code)
        paymentPurpose = \(paymentPurpose)
        """
    }
}

enum NbuQrParseError: Error, LocalizedError {
    case wrongHeader
    case errorVersionParsing
    case unsupportedVersion
    case errorCodingParsing
    case unsupportedCoding
    case errorFNAParsing
    case errorACPParsing
    case qrCodeError
    case unknown
    
    var errorDescription: String? {
        switch self {
        case .wrongHeader: return "Wrong header"
        case .errorVersionParsing: return "Can't parse version"
        case .unsupportedVersion: return "Unsupported version"
        case .errorCodingParsing: return "Can't parse coding"
        case .unsupportedCoding: return "Unsupported coding"
        case .errorFNAParsing: return "Error parsing of function, name or account"
        case .errorACPParsing: return "Error parsing of amount, code or payment purpose"
        case .qrCodeError: return "QrCode error"
        case .unknown: return "unknown error"
        }
    }
}

class NbuQrParser {
    typealias NbuQrV1StableContent = (function: String, fullName: String, account: String)
    typealias NbuQrV1VariableContent = (amount: NbuAmount?, code: String, paymentPurpose: String)
    
    private enum Constants {
        static let header: String = "BCD"
        static let versionFieldLength = 3
        static let supportedVersion: Int = 1
        static let supportedCoding: Int = 1
        static let functionLength: Int = 3
    }
    
    private var lastError: NbuQrParseError = .unknown
    
    private lazy var headerParser = CommonBaseParsers.stringParser.addSkipCharacter()
    
    private lazy var versionParser = Parser<Int> { str in
        let versionSubStr = str.prefix(Constants.versionFieldLength)
        guard versionSubStr.count == Constants.versionFieldLength,
              let version = Int(versionSubStr) else {
            return nil
        }
        str.removeFirst(Constants.versionFieldLength)
        return version
    }
    
    private lazy var extendVersionParser = self.versionParser.addSkipCharacter()
    private lazy var codingParser = CommonBaseParsers.intParser.addSkipCharacter()
    
    private lazy var currencyParser = Parser<NbuCurrency> { str in
        guard str.count >= NbuCurrency.codeLength,
              let currency = NbuCurrency(rawValue: String(str.prefix(NbuCurrency.codeLength))) else {
            return nil
        }
        str.removeFirst(NbuCurrency.codeLength)
        return currency
    }
    
    private lazy var nbuCurrencyParser = zip(currencyParser, CommonBaseParsers.decimalParser).map {
        NbuAmount.init(currency: $0.0, amount: $0.1)
    }
    
    private lazy var requiredStringParser = CommonBaseParsers.stringParser.setRequiredField()
    
    private lazy var stableV1ContentParser = Parser<NbuQrV1StableContent> { str in
        let functionParser = self.requiredStringParser
            .setLengthValidator(length: Constants.functionLength)
            .addSkipCharacter()
            .addSkipCharacter()
        let fullNameParser = self.requiredStringParser.addSkipCharacter()
        let zip1 = zip(functionParser, fullNameParser)
        let accountParser = self.requiredStringParser.addSkipCharacter()
        let result = zip(zip1, accountParser).map { (arg0) -> NbuQrV1StableContent in
            let ((f, n), a) = arg0
            return NbuQrV1StableContent(function: f, fullName: n, account: a)
        }
        return result.run(&str)
    }
    
    lazy var contentWithCurrencyV1Parser = Parser<NbuQrV1VariableContent> { str in
        let codeParser = self.requiredStringParser.addSkipCharacter().addSkipCharacter().addSkipCharacter()
        let zip1 = zip(self.nbuCurrencyParser.addSkipCharacter(), codeParser)
        let result = zip(zip1, self.requiredStringParser).map { (arg0) -> NbuQrV1VariableContent in
            let ((currency, code), p) = arg0
            return NbuQrV1VariableContent(amount: currency, code: code, paymentPurpose: p)
        }
        return result.run(&str)
    }
    
    lazy var contentWithoutCurrencyV1Parser = Parser<NbuQrV1VariableContent> { str in
        let codeParser = self.requiredStringParser.addSkipCharacter().addSkipCharacter().addSkipCharacter()
        let zip1 = zip(CommonBaseParsers.charParser, codeParser)
        let result = zip(zip1, self.requiredStringParser).map { (arg0) -> NbuQrV1VariableContent in
            let ((_, code), p) = arg0
            return NbuQrV1VariableContent(amount: nil, code: code, paymentPurpose: p)
        }
        return result.run(&str)
    }
    
    private lazy var qrContentV1Parser = Parser<NbuQrV1VariableContent> { str in
        str.prefix { !$0.isNewline }.isEmpty ? self.contentWithoutCurrencyV1Parser.run(&str) : self.contentWithCurrencyV1Parser.run(&str)
    }

    private lazy var parser = Parser<NbuQrCode> { str in
        self.lastError = .unknown
        
        guard let header = self.headerParser.run(&str),
              header == Constants.header else {
            self.lastError = .wrongHeader
            return nil
        }
        
        guard let version = self.extendVersionParser.run(&str) else {
            self.lastError = .errorVersionParsing
            return nil
        }
        
        guard version == Constants.supportedVersion else {
            self.lastError = .unsupportedVersion
            return nil
        }
        
        guard let coding = self.codingParser.run(&str) else {
            self.lastError = .errorCodingParsing
            return nil
        }
        
        guard coding == Constants.supportedCoding else {
            self.lastError = .unsupportedCoding
            return nil
        }
        
        guard let stableContent = self.stableV1ContentParser.run(&str) else {
            self.lastError = .errorFNAParsing
            return nil
        }
        guard let variableContent = self.qrContentV1Parser.run(&str) else {
            self.lastError = .errorACPParsing
            return nil
        }
        guard str.isEmpty else {
            self.lastError = .qrCodeError
            return nil
        }
        
        return NbuQrCode(version: version,
                         coding: coding,
                         function: stableContent.function,
                         fullName: stableContent.fullName,
                         account: stableContent.account,
                         amount: variableContent.amount,
                         code: variableContent.code,
                         paymentPurpose: variableContent.paymentPurpose)
    }
    
    init() {}
    
    func parse(_ string: String) -> Result<NbuQrCode, Error> {
        var data = string
        guard let result = parser.run(&data) else {
            return .failure(lastError)
        }
        return .success(result)
    }
}
