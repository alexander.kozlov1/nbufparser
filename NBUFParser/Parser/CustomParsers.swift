//
//  CustomParsers.swift
//  NBUFParser
//
//  Created by Alexander Kozlov on 10.04.2021.
//

import Foundation

class CommonBaseParsers {
    static func literalParser(_ p: String) -> Parser<Void> {
        Parser<Void> { str in
            guard str.hasPrefix(p) else { return nil }
            str.removeFirst(p.count)
            return ()
        }
    }
    
    static var charParser = Parser<Character> { str in
        str.isEmpty ? nil : str.removeFirst()
    }
    
    static var intParser = Parser<Int> { str in
        let prefix = str.prefix { $0.isNumber }
        let match = Int(prefix)
        str.removeFirst(prefix.count)
        return match
    }

    static var stringParser = Parser<String> { str in
        let prefix = str.prefix { !$0.isNewline }
        let result = String(prefix)
        str.removeFirst(prefix.count)
        return result
    }
    
    static var decimalParser = Parser<NSDecimalNumber> { str in
        let prefix = str.prefix { !$0.isNewline && !$0.isWhitespace }
        let result = NSDecimalNumber(string: String(prefix))
        guard result != NSDecimalNumber.notANumber else { return nil }
        str.removeFirst(prefix.count)
        return result
    }
    
    static var isCurrentLineEmpty = Parser<Bool> { str in
        str.prefix { !$0.isNewline }.isEmpty
    }
    
    static var currentLine = Parser<String> { str in
        String(str.prefix { !$0.isNewline })
    }
}

extension Parser {
    func addSkipCharacter() -> Parser<T> {
        zip(self, CommonBaseParsers.charParser).map { $0.0 }
    }
    
    func setRequiredField() -> Parser<T> {
        CommonBaseParsers.isCurrentLineEmpty.flatMap {
            $0 ? .never() : self
        }
    }
    
    func setLengthValidator(length: Int) -> Parser<T> {
        CommonBaseParsers.currentLine.flatMap {
            $0.count == length ? self : .never()
        }
    }
}
