//
//  ScannerViewController.swift
//  NBUFParser
//
//  Created by Alexander Kozlov on 17.04.2021.
//

import AVFoundation
import UIKit

class ScannerViewController: UIViewController {
    private struct Constants {
        static let barcodeTypes: [AVMetadataObject.ObjectType] = [.qr]
    }
    
    @IBOutlet private weak var cancelButton: UIBarButtonItem!
    @IBOutlet private var videoPreviewView: VideoPreviewView!
    @IBOutlet private var captureSession: AVCaptureSession!
    
    private let sessionQueue = DispatchQueue(label: AVCaptureSession.self.description(), attributes: [], target: nil)
    
    private var isQrCodeScanned = false
    var onQrCodeScanned: ((String) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        requestVideoPermission { [weak self] in
            self?.sessionQueue.async {
                self?.captureSession.startRunning()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        sessionQueue.async { [weak self] in
            self?.captureSession.stopRunning()
        }
    }
    
    // MARK: - Private
    
    private func setup() {
        videoPreviewView.videoPreviewLayer.session = captureSession
        sessionQueue.async { [weak self] in
            self?.configureCaptureSession()
        }
    }
    
    private func configureCaptureSession() {
        captureSession.beginConfiguration()
        captureSession.sessionPreset = .photo
        captureSession.usesApplicationAudioSession = false
        captureSession.automaticallyConfiguresApplicationAudioSession = false
        
        guard let videoDevice = AVCaptureDevice.default(for: .video),
            let videoDeviceInput = try? AVCaptureDeviceInput(device: videoDevice),
            captureSession.canAddInput(videoDeviceInput) else {
                captureSession.commitConfiguration()
                return
        }
        captureSession.addInput(videoDeviceInput)
        let metadataOutput = AVCaptureMetadataOutput()
        if captureSession.canAddOutput(metadataOutput) {
            captureSession.addOutput(metadataOutput)
            metadataOutput.metadataObjectTypes = Constants.barcodeTypes
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        }
        captureSession.commitConfiguration()
    }
    
    private func processScannedQrCode(_ barcode: String) {
        guard !isQrCodeScanned else { return }
        isQrCodeScanned = true
        onQrCodeScanned?(barcode)
        dismiss(animated: true, completion: nil)
    }
    
    private func requestVideoPermission(onSuccess: @escaping (() -> Void)) {
        AVCaptureDevice.requestAccess(for: .video) { response in
            DispatchQueue.main.async {
                guard response else {
                    let alert = UIAlertController(title: "Camera", message: "App needs permissions to camera", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { _ in
                        UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                    }))
                    
                    self.present(alert, animated: true)
                    return
                }
                onSuccess()
            }
        }
    }
        
    // MARK: - Actions
    
    @IBAction func onCancelTap(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

extension ScannerViewController: AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        guard !metadataObjects.isEmpty,
            let metadataObject = metadataObjects.first as? AVMetadataMachineReadableCodeObject,
            let qrCode = metadataObject.stringValue else { return }
        DispatchQueue.main.async {
            self.processScannedQrCode(qrCode)
        }
    }
}
