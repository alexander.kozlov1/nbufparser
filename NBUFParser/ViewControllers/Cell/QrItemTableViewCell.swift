//
//  QrItemTableViewCell.swift
//  NBUFParser
//
//  Created by Alexander Kozlov on 17.04.2021.
//

import UIKit

typealias QrItemCellCallbackModel = (type: QrItemRow, text: String, isCellSizeChanged: Bool)

class QrItemTableViewCell: UITableViewCell {
    static let cellId = "QrItemCellId"
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var dataTextView: UITextView!
    
    private var item: QrItemRow?
    private var onChange: ((QrItemCellCallbackModel) -> Void)?
    
    func configure(item: QrItemRow, value: String, onChange: ((QrItemCellCallbackModel) -> Void)?) {
        self.item = item
        self.onChange = onChange
        titleLabel.text = item.title
        dataTextView.text = value
        dataTextView.textAlignment = item.dataTextAlignment
        dataTextView.isEditable = onChange != nil
        titleLabel.isHidden = item.isTitleHidden
    }
}

// MARK: - UITextViewDelegate

extension QrItemTableViewCell: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        let size = textView.bounds.size
        let newSize = textView.sizeThatFits(CGSize(width: size.width,
                                                   height: CGFloat.greatestFiniteMagnitude))
        guard let item = item else { return }
        let isCellSizeChanged = size.height != newSize.height
        onChange?((type: item,
                   text: textView.text,
                   isCellSizeChanged: isCellSizeChanged))
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}
