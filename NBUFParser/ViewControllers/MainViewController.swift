//
//  ViewController.swift
//  NBUFParser
//
//  Created by Alexander Kozlov on 10.04.2021.
//

import UIKit

enum QrItemRow: Int, CaseIterable, RawRepresentable {
    case serviceMark
    case formatVersion
    case coding
    case function
    case bic
    case receiver
    case account
    case amount
    case receiverCode
    case target
    case reference
    case paymentPurpose
    case result
    
    var title: String? {
        switch self {
        case .serviceMark: return "Службова мітка"
        case .formatVersion: return "Версія формату"
        case .coding: return "Кодування"
        case .function: return "Функція"
        case .bic: return "BIC"
        case .receiver: return "Отримувач"
        case .account: return "Рахунок отримувача"
        case .amount: return "Сума/валюта"
        case .receiverCode: return "Код отримувача"
        case .target: return "Ціль"
        case .reference: return "Reference"
        case .paymentPurpose: return "Призначення платежу"
        case .result: return nil
        }
    }
    
    var isTitleHidden: Bool {
        self == .result
    }
    
    var dataTextAlignment: NSTextAlignment {
        self == .result ? .left : .right
    }
    
    static var sourceSectionAmount: Int {
        QrItemRow.allCases.count - 1
    }
}

class MainViewController: UIViewController {
    private enum Constants {
        static let qr1: String = """
        BCD
        001
        1
        UCT
         
        ПрАТ АК «Водопостачання»
        UA783226690000026005012107132
        UAH576.45
        40723825
         
         
        Сплата за червень 2019, вул. Свободи 1, кв. 24, Петренко С.І. Показання лічильника 23578,3
        """
        static let sourceSectionIndex: Int = 0
    }
    
    private var qrStringArray: [String]!
    @IBOutlet weak var tableView: UITableView!
    private lazy var nbuQrParser = NbuQrParser()
    private var scannedNbuQrCodeResult: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        qrStringArray = Constants.qr1.split(separator: "\n").map { String($0).trimmingCharacters(in: .whitespaces)
        }
    }
    
    private func onQrCellCallback(_ model: QrItemCellCallbackModel) {
        qrStringArray[model.type.rawValue] = model.text
        guard model.isCellSizeChanged else { return }
        UIView.setAnimationsEnabled(false)
        tableView.beginUpdates()
        tableView.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
    
    private func parseQrCode(_ data: String) {
        let result = nbuQrParser.parse(data)
        switch result {
        case .success(let parsed):
            scannedNbuQrCodeResult = parsed.debugDescription
            print("success:\n\(parsed.debugDescription)")
        case .failure(let error):
            scannedNbuQrCodeResult = "error: \(error.localizedDescription)"
            print("error: \(error.localizedDescription)")
        }
        tableView.reloadData()
    }
    
    private func row(for indexPath: IndexPath) -> QrItemRow? {
        indexPath.section == Constants.sourceSectionIndex ? QrItemRow(rawValue: indexPath.row) : .result
    }
    
    private func processScannedQrCode(_ qrCode: String) {
        qrCode.replacingOccurrences(of: "\n", with: " \n")
            .split(separator: "\n")
            .enumerated().forEach {
                qrStringArray[$0] = String($1).trimmingCharacters(in: .whitespaces)
            }
        parseQrCode(qrCode)
    }
    
    @IBAction private func onParseTap(_ sender: Any) {
        let data = qrStringArray.joined(separator: "\n")
        parseQrCode(data)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let vc = segue.destination as? ScannerViewController else { return }
        vc.onQrCodeScanned = { [weak self] qrCode in
            self?.processScannedQrCode(qrCode)
        }
    }
}

// MARK: - UITableViewDataSource

extension MainViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        (scannedNbuQrCodeResult == nil) ? 1 : 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        section == Constants.sourceSectionIndex ? QrItemRow.sourceSectionAmount : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: QrItemTableViewCell.cellId, for: indexPath) as? QrItemTableViewCell,
              let itemRow = row(for: indexPath) else {
            return UITableViewCell()
        }
        if indexPath.section == Constants.sourceSectionIndex {
            cell.configure(item: itemRow, value: qrStringArray[indexPath.row]) { [weak self] model in
            self?.onQrCellCallback(model)
            }
        } else {
            cell.configure(item: itemRow, value: scannedNbuQrCodeResult ?? "no result", onChange: nil)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        section == Constants.sourceSectionIndex ? "Source" : "Result"
    }
}
