//
//  ViewController.swift
//  NBUFParser
//
//  Created by Alexander Kozlov on 10.04.2021.
//

import UIKit

enum QrItemRow: Int, CaseIterable, RawRepresentable {
    case serviceMark
    case formatVersion
    case coding
    case function
    case bic
    case receiver
    case account
    case amount
    case receiverCode
    case target
    case reference
    case paymentPurpose
    case display
    
    var title: String {
        switch self {
        case .serviceMark: return "Службова мітка"
        case .formatVersion: return "Версія формату"
        case .coding: return "Кодування"
        case .function: return "Функція"
        case .bic: return "BIC"
        case .receiver: return "Отримувач"
        case .account: return "Рахунок отримувача"
        case .amount: return "Сума/валюта"
        case .receiverCode: return "Код отримувача"
        case .target: return "Ціль"
        case .reference: return "Reference"
        case .paymentPurpose: return "Призначення платежу"
        case .display: return "Відображення"
        }
    }
}

class MainViewController: UIViewController {
    private enum Constants {
        static let qr1: String = """
        BCD
        001
        1
        UCT
         
        ПрАТ АК «Водопостачання»
        UA783226690000026005012107132
        UAH576.45
        40723825
         
         
        Сплата за червень 2019, вул. Свободи 1, кв. 24, Петренко С.І. Показання лічильника 23578,3
         
        """
        static let qr2: String = """
        BCD
        001
        1
        UCT

        ТОВ «Будматеріали»
        UA783226690000026005012107133
        UAH124.45
        40723823


        Сплата за цемент М500

        """
        static let qr3: String = """
        BCD
        001
        1
        UCT

        ТОВ «Стоматологія»
        UA783226690000026005012107358
        
        40723824


        Стоматологічні послуги

        """
        static let qr4: String = """
        BCD
        001
        1
        UCT

        Шевченко Тарас Петрович
        UA783226690000026205012107136
        UAH150.00
        3045312215


        Повернення боргу за обід у кафе

        """
        static let qrArray = [Constants.qr1, Constants.qr2, Constants.qr3, Constants.qr4]
    }
    
    private var qrStringArray: [String]!
    @IBOutlet weak var tableView: UITableView!
    private lazy var nbuQrParser = NbuQrParser()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        qrStringArray = Constants.qr1.split(separator: "\n").map { String($0).trimmingCharacters(in: .whitespaces) }
        
        Constants.qrArray.forEach {
            let result = nbuQrParser.parse($0)
            switch result {
            case .success(let parsed):
                print("success: \(parsed)")
            case .failure(let error):
                print("error \(error.localizedDescription)")
            }
        }
    }
    
    private func onQrCellCallback(_ model: QrItemCellCallbackModel) {
        qrStringArray[model.type.rawValue] = model.text
        guard model.isCellSizeChanged else { return }
        UIView.setAnimationsEnabled(false)
        tableView.beginUpdates()
        tableView.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
    
    @IBAction private func onParseTap(_ sender: Any) {
        let data = qrStringArray.joined(separator: "\n")
        let result = nbuQrParser.parse(data)
        switch result {
        case .success(let parsed):
            print("success: \(parsed)")
        case .failure(let error):
            print("error \(error.localizedDescription)")
        }
    }
}

extension MainViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        QrItemRow.allCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: QrItemTableViewCell.cellId, for: indexPath) as? QrItemTableViewCell,
              let itemRow = QrItemRow(rawValue: indexPath.row) else {
            return UITableViewCell()
        }
        cell.configure(item: itemRow, value: qrStringArray[indexPath.row]) { [weak self] model in
            self?.onQrCellCallback(model)
        }
        return cell
    }
}
